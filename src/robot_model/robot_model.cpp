#include <fmt/core.h>
#include <umrob/robot_model.h>

#include <urdf_model/pose.h>
#include <urdf_parser/urdf_parser.h>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <stdexcept>

#include <iostream>

namespace umrob {
RobotModel::RobotModel(const std::string& urdf) {
    //std::map<std::string, double> joints_position_;
    //std::map<std::string, Eigen::Affine3d> links_pose_;
    model_ = urdf::parseURDF(urdf);
    auto root = model_->getRoot();    //modle_getName: nao_arms
    auto joints = root->child_joints; //joints[0]: LShoulderPitch  //joints[1]: RShoulderPitch
    auto links = root->child_links;   //links[0]: LShoulder  //links[1]: RShoulder
    //auto LShoulder = links[0];
    //auto RShoulder = joints[1]->;
    
    links_pose_.clear();
    Eigen::Affine3d eigMa;
    //eigMa = Eigen::Affine3d::Identity(); 
    auto presentLinks = links;

    std::vector<urdf::LinkSharedPtr>::iterator preLink_itr;
    std::vector<std::vector<urdf::LinkSharedPtr>::iterator> itrStack;
    itrStack.push_back(presentLinks.begin());
    itrStack.push_back(presentLinks.end());
    while (!itrStack.empty()) {
        //++jointIndex;
        auto preLink_itr_end = itrStack.back();
        itrStack.pop_back();
        preLink_itr = itrStack.back();
        itrStack.pop_back();
        links_pose_.insert(std::make_pair((*preLink_itr)->name, eigMa));
        joints_position_.insert(std::make_pair((*preLink_itr)->parent_joint->name, 0.0));
        jspVec.push_back((*preLink_itr)->parent_joint);

        if (preLink_itr != preLink_itr_end - 1) {
            itrStack.push_back(preLink_itr + 1);
            itrStack.push_back(presentLinks.end());
        }
        if (!(*preLink_itr)->child_links.empty()) {       
            presentLinks = (*preLink_itr)->child_links;
            itrStack.push_back(presentLinks.begin());
            itrStack.push_back(presentLinks.end());
        }
    }
    /*
    for (std::map<std::string, Eigen::Affine3d>::iterator it = links_pose_.begin(); it != links_pose_.end(); ++it) {
        std::cout << it->first << std::endl;
    }
    */
}

double& RobotModel::jointPosition(const std::string& name) {
    return joints_position_.at(name);
}

size_t RobotModel::jointIndex([[maybe_unused]] const std::string& name) const {
    //not the same order as insertion, sorted by keys value
    size_t jointIndex = distance(joints_position_.begin(), joints_position_.find(name)); // begin with zero
    std::cout << jointIndex << std::endl;
    return jointIndex;
}

const std::string& RobotModel::jointNameByIndex([
    [maybe_unused]] size_t index) const {
    return joints_position_.begin()->first; // TODO: implement
    //Not sure
}

Eigen::Vector3d to_vector3d(urdf::Vector3 v) {
    return Eigen::Vector3d(v.x, v.y, v.z);
}

Eigen::Quaterniond to_quat(urdf::Rotation q) {
    return Eigen::Quaterniond(q.w, q.x, q.y, q.z);
}
void RobotModel::update() {
    for (size_t idx = 0; idx < jspVec.size(); ++idx) {
        auto joint = jspVec[idx];
        auto origin = joint->parent_to_joint_origin_transform;
        Eigen::Affine3d joint_pose;
        joint_pose.translation() = to_vector3d(origin.position);
        joint_pose.linear() = to_quat(origin.rotation).toRotationMatrix(); // static // parent to joint origin transform : (i-1)T(i)

        auto axis = to_vector3d(joint->axis);
        auto joint_transform = Eigen::Affine3d::Identity();
        joint_transform.linear() = Eigen::AngleAxisd(joints_position_.at(joint->name), axis).toRotationMatrix(); //dynamic

        if (links_pose_.find(joint->parent_link_name)!= links_pose_.end()) { // parent link exists
            joint_pose = links_pose_.at(joint->parent_link_name)*joint_pose*joint_transform;
        } else {
            joint_pose = joint_pose*joint_transform;
        }
        links_pose_[joint->child_link_name] = joint_pose;
        //std::cout << joint_pose.matrix() << std::endl;
        //std::cout << joint->child_link_name << std::endl;

    }
}

const Eigen::Affine3d&
RobotModel::linkPose(const std::string& link_name) const {
    return links_pose_.at(link_name);
}

const Jacobian& RobotModel::linkJacobian([
    [maybe_unused]] const std::string& link_name) {
    // TODO: implement
    Jacobian jac;
    auto link = model_->getLink(link_name);
    auto presentLink = link;
    for (; links_pose_.find(presentLink->name) != links_pose_.end(); ){  //go find root link
        jac.joints_name.push_back(presentLink->parent_joint->name);
        jac.joints_chain.push_back(model_->getJoint(presentLink->parent_joint->name));
        presentLink = presentLink->getParent();
    }
    std::reverse(jac.joints_chain.begin(),jac.joints_chain.end()); 
    std::reverse(jac.joints_name.begin(),jac.joints_name.end()); 

    //for (std::vector<std::string>::iterator it = jac.joints_name.begin(); it != jac.joints_name.end(); ++it) {
    //    std::cout << *it << std::endl;
    //}

    for (std::vector<urdf::JointConstSharedPtr>::iterator it = jac.joints_chain.begin(); it != jac.joints_chain.end(); ++it) {

        Eigen::Vector3d axR = to_vector3d((*it)->axis);   // cause urdf dosent assigne aixs z as the rotation axis
        std::ptrdiff_t rotAx;
        double maxOfV = axR.maxCoeff(&rotAx);             // find rotation axis 
        Eigen::Vector3d zi = links_pose_.at((*it)->child_link_name).matrix().block(0, rotAx, 3, 1);
        Eigen::Vector3d li = links_pose_.at(link_name).matrix().block(0, 3, 3, 1) - links_pose_.at((*it)->child_link_name).matrix().block(0, 3, 3, 1);
        rotAx = maxOfV;     // for disapearing the warnings
        /////////////////////////////////////////////////////////////////////

        auto matCros = zi.cross(li);

        Eigen::Matrix<double, 6, 1> Ji;
        Ji << matCros, zi;

        jac.matrix.conservativeResize(6, jac.matrix.cols()+1);
        jac.matrix.col(jac.matrix.cols()-1) << Ji;
        //std::cout << jac.matrix << std::endl;
        //std::cout << "xxxxxxxxxxxxxjac.matrix" << std::endl;
    }

    links_jacobian_[link_name] = jac;
    return links_jacobian_.at(link_name);
}

const JointLimits&
RobotModel::jointLimits(const std::string& joint_name) const {
    return joints_limits_.at(joint_name);
}

size_t RobotModel::degreesOfFreedom() const {
    // TODO: implement
    return 0;
}

std::vector<urdf::JointConstSharedPtr> RobotModel::jacobianJointChain([
    [maybe_unused]] const std::string& link_name) const {
    std::vector<urdf::JointConstSharedPtr> joint_chain;
    // TODO: implement
    return joint_chain;
}

} // namespace umrob