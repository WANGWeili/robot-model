//! \file robot_model.h
//! \author Benjamin Navarro
//! \brief Declaration of the RobotModel class
//! \date 08-2020

#pragma once

#include <Eigen/Dense>
#include <urdf_parser/urdf_parser.h>

#include <limits>
#include <map>
#include <string>
#include <type_traits>

namespace umrob {

//! \brief Hold the position, velocity and effort limits of a joint
//!
// clang-format off
struct JointLimits {
    double min_position{-std::numeric_limits<double>::infinity()}; //!< Minimum joint position (rad) 
    double max_position{std::numeric_limits<double>::infinity()};  //!< Maximum joint position (rad)
    double max_velocity{std::numeric_limits<double>::infinity()};  //!< Maximum joint velocity (rad/s)
    double max_effort{std::numeric_limits<double>::infinity()};    //!< Maximum joint effort (Nm)
};
// clang-format on

//! \brief Hold a 6xDofs Jacobian matrix and its associated list of joints
//!
// clang-format off
struct Jacobian {
    Eigen::Matrix<double, 6, Eigen::Dynamic> matrix; //!< The Jacobian matrix
    std::vector<std::string> joints_name; //!< Names of the joints associated with each column of
                                          //!< the matrix, in the same order
    std::vector<urdf::JointConstSharedPtr> joints_chain; //!< Joints involved in the Jacobian                                           
};
// clang-format on

//! \brief Handle computations related to a robot model: forward kinematics,
//! jacobian extraction, etc
//!
class RobotModel {
public:
    template <typename ContainerT> class ContainerView {
    public:
        explicit ContainerView(ContainerT& container) : container_{container} {
        }

        typename ContainerT::const_iterator begin() const {
            return container_.begin();
        }

        template <typename T = ContainerT>
        std::enable_if_t<not std::is_const_v<T>, typename ContainerT::iterator>
        begin() {
            return container_.begin();
        }

        typename ContainerT::const_iterator end() const {
            return container_.end();
        }

        template <typename T = ContainerT>
        std::enable_if_t<not std::is_const_v<T>, typename ContainerT::iterator>
        end() {
            return container_.end();
        }

    private:
        ContainerT& container_;
    };

    using JointsPositionView = ContainerView<std::map<std::string, double>>;
    using JointsPositionConstView =
        ContainerView<const std::map<std::string, double>>;

    //! \brief Construct a RobotModel using a URDF description
    //!
    //! \param urdf the content of the URDF model
    explicit RobotModel(const std::string& urdf);

    //! \brief Use the current joint state to update the robot state
    //!
    void update();

    double& jointPosition(const std::string& name);

    JointsPositionConstView jointsPosition() const {
        return JointsPositionConstView{joints_position_};
    }

    JointsPositionView jointsPosition() {
        return JointsPositionView{joints_position_};
    }

    size_t jointIndex(const std::string& name) const;
    const std::string& jointNameByIndex(size_t index) const;

    //! \return Eigen::Affine3d The link pose in the root frame
    const Eigen::Affine3d& linkPose(const std::string& link_name) const;

    //! \brief Get the Jacobian matrix, in the root frame, of the given link
    //!
    //! \param link_name Name of the link
    //! \return Jacobian The link's Jacobian in the root frame
    const Jacobian& linkJacobian(const std::string& link_name);

    //! \brief Get the limits of the given joint
    //!
    //! \param joint_name Name of the joint
    //! \return JointLimits The joint limits
    const JointLimits& jointLimits(const std::string& joint_name) const;

    //! \brief Gives the robot number of degrees of freedom. Might be different
    //! from the number of joints
    //!
    //! \return size_t Number of degrees of freedom
    size_t degreesOfFreedom() const;

    std::vector<urdf::JointConstSharedPtr>
    jacobianJointChain(const std::string& link_name) const;

    bool hasJoint(const std::string& name) const;
    bool hasLink(const std::string& name) const;

private:
    urdf::ModelInterfaceSharedPtr model_;
    std::map<std::string, double> joints_position_;
    std::map<std::string, Eigen::Affine3d> links_pose_;
    std::map<std::string, JointLimits> joints_limits_;
    std::map<std::string, Jacobian> links_jacobian_;
    std::vector<urdf::JointSharedPtr> jspVec;
    size_t degrees_of_freedom_{0};
};

} // namespace umrob